$global:baseUrl = "https://foundryvtt.com"
$global:loginUrl = "${global:baseUrl}/auth/login/"
$global:websession = New-Object Microsoft.PowerShell.Commands.WebRequestSession

function getCsrfMiddlewareToken {
    Write-Host "Fetch main page for the CSRF token ..."
    $mainPage = Invoke-WebRequest $global:baseUrl -WebSession $global:websession
    Write-Host $global:websession.Cookies.GetCookies($global:baseUrl)
    Write-Host "Extract CSRF middleware token ..."
    $csrfMiddlewareToken = ($mainPage.InputFields | Where-Object name -eq 'csrfmiddlewaretoken' | Select-Object -first 1).value
    Write-Host $csrfMiddlewareToken
    return $csrfMiddlewareToken
}

function login {
    param (
        [Parameter(Mandatory=$true)]
        [String]$CsrfMiddlewareToken,
        [Parameter(Mandatory=$true)]
        [String]$Username,
        [Parameter(Mandatory=$true)]
        [SecureString]$Password
    )
    
    Write-Host "Filling form with given attributes ..."
    $Form = @{
        "csrfmiddlewaretoken"   = $CsrfMiddlewareToken
        "login_password"        = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`
                                    [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password))
        "login_redirect"        = "/"
        "login_username"        = $Username
        "login"                 = ""
    }
    $Headers = @{
        "DNT"                       = "1"
        "Referer"                   = "https://foundryvtt.com"
        "Upgrade-Insecure-Requests" = "1"
        "User-Agent"                = "Mozilla/5.0"
    }
    Write-Host "Logging in at ${global:loginUrl} ..."
    $loginPage = Invoke-WebRequest -Uri ${global:loginUrl} -Method "Post" -Body $Form -Headers $Headers -WebSession $global:websession
}

function fetchReleaseUrl {
    param (
        [Parameter(Mandatory=$true)]
        [String]$Version
    )
    $downloadLink = "${global:baseUrl}/releases/download?version=${version}&platform=windows"
    Write-Host "Fetching FVTT from ${downloadLink} ..."
    Write-Host $global:websession.Cookies.GetCookies($global:baseUrl)
    # Invoke-WebRequest -Uri $downloadLink -WebSession $global:websession
}

if (-not (Test-Path env:FVTT_USERNAME) -or -not (Test-Path env:FVTT_PASSWORD)) {
    Write-Warning "Environment variables for username (FVTT_USERNAME) and password (FVTT_PASSWORD) not set, exiting."
    exit
}

function main {
    param (
        [Parameter(Mandatory=$true)]
        [String]$Version
    )
    $CsrfMiddlewareToken = getCsrfMiddlewareToken
    $username = $env:FVTT_USERNAME
    $password = $env:FVTT_PASSWORD | ConvertTo-SecureString -AsPlainText -Force

    login -CsrfMiddlewareToken $CsrfMiddlewareToken -Username $username -Password $password
    fetchReleaseUrl -Version $Version
}

main -Version "0.7.5"
